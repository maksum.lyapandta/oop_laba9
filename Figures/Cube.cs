﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba9.Figures
{
    public class Cube : TrigonometricFigure
    {
        private double sideLength;

        public Cube(double sideLength)
        {
            this.sideLength = sideLength;
        }

        public override double GetVolume()
        {
            return Math.Pow(sideLength, 3);
        }
    }
}
