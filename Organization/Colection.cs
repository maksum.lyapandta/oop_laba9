﻿using System.Collections;

namespace OOP_Laba9.Organization
{
    public class OrganizationCollection : IEnumerable<Organization>
    {
        private List<Organization> organizations = new List<Organization>();

        public void AddOrganization(Organization org)
        {
            organizations.Add(org);
        }

        public IEnumerator<Organization> GetEnumerator()
        {
            return organizations.OrderByDescending(org => org.Rating).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
