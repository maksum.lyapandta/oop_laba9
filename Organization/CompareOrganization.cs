﻿
namespace OOP_Laba9.Organization
{
    public class OrganizationComparer : IComparer<Organization>
    {
        public int Compare(Organization x, Organization y)
        {
            int result = x.EmployeeCount.CompareTo(y.EmployeeCount);


            if (result == 0)
            {
                result = x.Rating.CompareTo(y.Rating);
            }

            return result;
        }
    }
}
